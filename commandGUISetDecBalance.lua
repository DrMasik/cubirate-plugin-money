--
-- commandGUISetDecBalance
--
--------------------------------------------------------------------------------

function commandGUISetDecBalance(aSplit, aPlayer)
-- /money set d Nikolya 300 c
-- Decrease player's balans by 300

  local func_name = 'commandGUISetDecBalance()';

  if aPlayer == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlayer", 2);
    return true;
  end

  if aSplit == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aSplit", 2);
    return true;
  end

  -- Check arguments count
  if #aSplit < 5 then
    aPlayer:SendMessageWarning(msgSetResiverAndSum);
    return true;
  end

  local receiverName = aSplit[4];

  -- Get payee ID
  local receiverID = gStorage:getPlayerID(receiverName);

  if receiverID == false or receiverID == 0 then
    aPlayer:SendMessageWarning(msgReceiverDoesNotExists);
    return true;
  end

  -- Get sum
  local sum = tonumber(aSplit[5]);

  -- Check money sum
  if sum == nil or sum == false or sum <= 0 then
    aPlayer:SendMessageWarning(msgSumNotSet);
    return true;
  end

  -- Get currency ID by currency code
  local currencyCodeName = 'c';
  local currencyID = gStorage:getCurrencyIDByCodeName(currencyCodeName);

  -- Get payer (player) ID
  local plName = aPlayer:GetName();
  local plID = gStorage:getPlayerID(plName);

  -- Create transaction
  if not gStorage:setDecreaseBalance(receiverID, currencyID, sum) then
    aPlayer:SendMessageWarning(msgTransactionAborted);
    return true;
  end

  aPlayer:SendMessageSuccess(msgTransactionSuccess);

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
