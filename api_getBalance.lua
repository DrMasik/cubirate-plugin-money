--
-- api_getBalance
--
--------------------------------------------------------------------------------

function api_getBalance(aPlayerName, aCurrenyName)
-- Return player's balance and function status.
-- aCurrenyName default must have 'Credits'
-- Function return code of error if return status false
--
-- Simple function how to use
-- function commandGUIGetBalance()
--   local status, value = cPluginManager:CallPlugin("Money", "api_getBalance", "DrMasik", "Credits")
--
--   if status == true then
--     console_log("TRUE")
--     console_log(value)
--   else
--     console_log("FALSE")
--     console_log("Returned error code: " + value)
--   end
-- end

  local func_name = 'api_getBalance()';

  if aPlayerName == nil then
    return false, 1;
  end

  if aCurrenyName == nil then
    aCurrenyName = 'Credits';
  end

  -- Set local variables
  local plName = aPlayerName;
  local plID = gStorage:getPlayerID(plName);

  -- Check is it player ID detected?
  if plID == false or plID == 0 then
    return false, 2;
  end

  -- Get balance list
  local balanceList = gStorage:getBalance(plID, nil);

  -- Is it balance exists
  if balanceList == false then
    return false, 3;
  end

  local balance = 0;

  if balanceList[aCurrenyName] ~= nil then
    balance = balanceList[aCurrenyName];
  end

  return true, balance;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
