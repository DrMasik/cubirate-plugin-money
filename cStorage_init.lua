--
--
--
--------------------------------------------------------------------------------

function cStorage:init()
-- Initialize an create DB

  local func_name = 'cStorage:init()';

  --
  -- List of kist
  --
  local sql =[=[
    --
    -- Turn on support foreign keys
    --
    PRAGMA foreign_keys = ON;

    -----------------------------------------------------------------
    -- Create table for worlds
    -----------------------------------------------------------------
    CREATE TABLE IF NOT EXISTS worlds(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT
    );

    CREATE UNIQUE INDEX IF NOT EXISTS worlds_id
    ON worlds(id);

    CREATE UNIQUE INDEX IF NOT EXISTS worlds_name
    ON worlds(name);

    -----------------------------------------------------------------
    -- Table of players name per world (in future)
    -----------------------------------------------------------------

    CREATE TABLE IF NOT EXISTS players(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      login TEXT NOT NULL
    );

    CREATE UNIQUE INDEX IF NOT EXISTS players_id
    ON players(id);

    CREATE UNIQUE INDEX IF NOT EXISTS players_login
    ON players(login);

    --
    -- List of curencies
    --
    CREATE TABLE IF NOT EXISTS currencies(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT NOT NULL,  -- Currency short name
      long_name TEXT,  -- Currency long (full) name
      code TEXT -- Corrency unique text code
    );

    CREATE UNIQUE INDEX IF NOT EXISTS currencies_id
    ON currencies(id);

    CREATE UNIQUE INDEX IF NOT EXISTS currencies_code
    ON currencies(code);

    --
    -- Player's balance
    --
    CREATE TABLE IF NOT EXISTS balances(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      player_id INTEGER NOT NULL,
      currency_id INTEGER NOT NULL,
      sum REAL DEFAULT 0,

      FOREIGN KEY(player_id) REFERENCES players(id),
      FOREIGN KEY(currency_id) REFERENCES currencies(id)
    );

    CREATE UNIQUE INDEX IF NOT EXISTS balances_id
    ON balances(id);

    CREATE UNIQUE INDEX IF NOT EXISTS balances_player_id_currency_id
    ON balances(player_id, currency_id);

    CREATE INDEX IF NOT EXISTS balances_player_id
    ON balances(player_id);

    --
    -- Save players trunsactions
    --
    CREATE TABLE IF NOT EXISTS transactions(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      pay_player_id INTEGER NOT NULL,
      sell_player_id INTEGER NOT NULL,
      currency_id INTEGER NOT NULL,
      sum REAL DEFAULT 0,

      FOREIGN KEY(pay_player_id) REFERENCES players(id),
      FOREIGN KEY(sell_player_id) REFERENCES players(id),
      FOREIGN KEY(currency_id) REFERENCES currencies(id)
    );

    CREATE UNIQUE INDEX IF NOT EXISTS transactions_id
    ON transactions(id);

    CREATE INDEX IF NOT EXISTS transactions_pay_player_id
    ON transactions(pay_player_id);

    CREATE INDEX IF NOT EXISTS transactions_sell_player_id
    ON transactions(sell_player_id);

    -----------------------------------------------------------------
    -- Table of banned items
    -----------------------------------------------------------------
    CREATE TABLE IF NOT EXISTS banned_items(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      item_id INTEGER
    );

    CREATE UNIQUE INDEX IF NOT EXISTS banned_items_id
    ON banned_items(id);

    CREATE UNIQUE INDEX IF NOT EXISTS banned_items_item_id
    ON banned_items(item_id);

    -----------------------------------------------------------------
    -- Table of chests
    -----------------------------------------------------------------

    -- DROP TABLE chests;

    CREATE TABLE IF NOT EXISTS chests(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      x INTEGER,
      y INTEGER,
      z INTEGER,
      world_id INTEGER,
      player_id INTEGER,

      FOREIGN KEY(player_id) REFERENCES players(id),
      FOREIGN KEY(world_id) REFERENCES worlds(id)
    );

    CREATE UNIQUE INDEX IF NOT EXISTS chests_id
    ON chests(id);

    CREATE INDEX IF NOT EXISTS chests_player_id
    ON chests(player_id);

    CREATE UNIQUE INDEX IF NOT EXISTS chests_world_id_player_id_x_y_z
    ON chests(world_id, player_id, x, y, z);

  ]=];

  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local ret = db:exec(sql);

  -- Is it allright?
  if ret ~= sqlite3.OK then
    console_log(func_name .." -> db:exec return code ".. ret, 2);
    db:close();
    return false;
  end

  -- Close DB
  db:close();

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

