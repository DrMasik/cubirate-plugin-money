--
-- processBuyBank();
--
--------------------------------------------------------------------------------

function processBuyBank(aPlayer, aPoint)
  local func_name = 'processBuyBank()';

  -- Check arguments
  if aPlayer == nil or aPoint == nil then
    return false;
  end

  local world = aPlayer:GetWorld();
  local seller = nil;
  local sellerName = '';
  local signPos = aPoint;
  local plSellerInventory = nil;  -- Seller inventory
  local plSellerItemCount = 0;
  local sellerID = 0;
  local buyer = aPlayer;
  local buyerBalance = 0;
  local buyerName = aPlayer:GetName();
  local buyerID = 0;

  -- get sign block
  local isSign, r1, r2, r3, r4 = world:GetSignLines(signPos.x, signPos.y, signPos.z);

  r4 = r4:lower();

  -- Get currency ID by currency code
  local currencyCodeName = 'c';
  local currencyID = gStorage:getCurrencyIDByCodeName(currencyCodeName);

  local itemID, itemIDExtended = signProcessR2(r2);

  -- Is it item ID founded?
  if itemID == nil or itemID == false or itemID == 0 then
    aPlayer:SendMessageWarning(msgItemCodeWrong);
    return false;
  end

  -- Get count and price
  local count, price = signProcessR3(r3);

  -- Is it all right
  if count == nil or count == false or price == nil then
    aPlayer:SendMessageWarning(msgSignCanNotGetCount);
    return false;
  end

  -- Check price < 0
  if price < 0 then
    aPlayer:SendMessageWarning(msgPriceLessThanZero);
    return false;
  end

  -- Check is it Game bank
  if r4 == 'bank of m-c.link' then
    -- Add items to bank :) For statistic
    seller = 0;
  else
    seller = getPlayer(world, r4);
  end

  sellerName = r4;

  -- Check seller exists?
  if seller == nil or seller == false then
    aPlayer:SendMessageWarning(msgSellerNotFound);
    return false;
  end

  -- Get player inventory
  -- plSellerInventory = seller:GetInventory();

  -- Get player items count
  -- plSellerItemCount = plSellerInventory:HowManyItems(cItem(itemID, 1, itemIDExtended));

  -- Check seller items count
  -- if plSellerItemCount < count then
  --   aPlayer:SendMessageWarning(msgYouDoNotHaveBloks);
  --   return false;
  -- end

  -- Get seller ID
  sellerID = gStorage:getPlayerID(sellerName);

  -- Check buyer balance
  buyerID = gStorage:getPlayerID(buyerName);
  buyerBalance = gStorage:getCurrncyBalance(buyerID, currencyID);

  if buyerBalance == false or buyerBalance == nil then
    aPlayer:SendMessageWarning(msgBuyerBalanceNotFount);
    return false;
  end

  if buyerBalance < price then
    aPlayer:SendMessageWarning(msgBuyerDoesNotHaveEnoughMoney);
    return false;
  end

  --
  -- Process sell transaction
  --

  -- Create transaction
  if not gStorage:addTransaction(buyerID, sellerID, currencyID, price) then
    aPlayer:SendMessageWarning(msgTransactionAborted);
    return false;
  end

  -- Create item object
  local itemObj = cItem(itemID, count, itemIDExtended);

  -- Check item creation
  if itemObj:IsEmpty() then
    aPlayer:SendMessageWarning(msgItemCodeWrong);
    return false;
  end

  -- Add items to buyer
  buyer:GetInventory():AddItem(itemObj);
  buyer:SendMessageSuccess(msgYouBuySuccess);

  -- Pickup items from seller
  -- pickUpItemsfromSeller(world, sellerName, itemID, count, itemIDExtended);

  return true;
end

