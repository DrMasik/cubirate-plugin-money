--
-- Plugin name: Money
-- cStorage:getChestList()
--

function cStorage:getChestList(aWorldID, aPlayerID)

  local func_name = "cStorage:getChestList()";

  -- Check arguments
  if aWorldID == nil or aPlayerID == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil, 2);
    return false;
  end

  local plID = aPlayerID;
  local worldID = aWorldID;
  local chestList = {};

  local sql = [[
    SELECT id
    FROM chests
    WHERE world_id = :worldID
          AND player_id = :plID
    ;
  ]];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plID = plID,
    worldID = worldID
  });

  -- Get chest ID
  for id1 in stmt:urows() do
    chestList[id1] = 0;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Allright
  return chestList;
end

