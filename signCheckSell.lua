--
--
--
--------------------------------------------------------------------------------

function signCheckSell(aSignID)
  local func_name = 'signCheckSell()';

  -- console_log(func_name .." -> begin function", 1);  -- Debug

  if aSignID == nil then
    -- console_log(func_name .." -> aSignID == nil", 1);  -- Debug
    return false;
  end

  local id = aSignID;
  local ownerName = gSign[id]:getOwnerName();
  local worldName = gSign[id]:getWorldName();
  local world = nil;
  local pos = {};
  local count = 0;
  local price = 0;
  local itemID = 0;
  local itemIDExtended = 0;

  -- Get sign coordinates
  pos.x = gSign[id]:getPosX();
  pos.y = gSign[id]:getPosY();
  pos.z = gSign[id]:getPosZ();

  -- Find world
  world = worldFind(worldName);

  if world == nil or world == false then
    -- console_log(func_name .." -> world == nil or world == false", 1);  -- Debug
    return false;
  end

  local signExists, r1, r2, r3, r4 = world:GetSignLines(pos.x, pos.y, pos.z);

  -- Check sign exists
  if signExists == false then
    -- console_log(func_name .." -> signExists == false", 1);  -- Debug
    return false;
  end

  -- Chec sign type
  if r1 == nil or r1 ~= '[sell]' then
    -- console_log(func_name .." -> r1 == nil or r1 ~= sell", 1); -- Debug
    return false;
  end

  -- Check owner of sign
  if r4 == nil or r4:lower() ~= ownerName:lower() then
    -- console_log(func_name .." -> r4 == nil or r4:lower() ~= ownerName:lower()", 1); -- Debug
    return false;
  end

  -- Get and check itemID
  itemID, itemIDExtended = signProcessR2(r2);
  if itemID == nil or itemID == false then
    -- console_log(func_name .." -> itemID == nil or itemID == false", 1);  -- Debug
    return false;
  end

  -- Get and check item count and price
  count, price = signProcessR3(r3);
  if count == nil or count == false then
    -- console_log(func_name .." -> count == nil or count == false", 1);  -- Debug
    return false;
  end

  return true;
end
--------------------------------------------------------------------------------

