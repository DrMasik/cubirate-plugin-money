--
--
--
--------------------------------------------------------------------------------

-- msgBuyerBalanceNotFount = "Buyer balance not found";
msgBuyerBalanceNotFount = "Не могу определить баланс покупателя";
-- msgBuyerDoesNotHaveEnoughMoney = "The buyer does not have enough money";
msgBuyerDoesNotHaveEnoughMoney = "У покупателя нехватает кредитов";
-- msgBuyerNotFount = "Buyer not fount";
msgBuyerNotFount = "Покупатель не найден";
msgCanNotGetCurrencies = "Не могу получить список валют";
msgCanNotGetCurrencyBalance = "Не могу получить баланс по валюте";
msgCanNotGetPlayerID = "Не могу получить ID игрока";

msgChestDeleteSuccess = "Chest deleted from list";
msgChestDeleteUsage = "Use /money chest delete <id>";
msgChestIDNotFound = "Chest ID not found";
msgChestListAreEmpty = "У Вас нет выделенных сундуков";
msgChestMarkStatusActivated = "Выделите сундуки";
msgChestMarkStatusDeactivated = "Пометка сундуков отключена";
msgChestNotDeleted = "Can not delete chest record";
msgChestAdded = "Сундук добавлен в список";

-- msgSignCanNotGetCount = "Can not get count from sign";
msgSignCanNotGetCount = "Не могу определить количество на указателе";
msgDoNotHaveMoney = "У Вас нет денег";
msgItemDisabled = "Блок запрещен к продаже";
msgItemCodeWrong = "Не верный код блока";
msgItemCountWronga = "Ошибка в количестве";
msgPayeeDoesNotExists = "Получатель не найден";
msgPayeeOffline = "Игрок не в игре. Вы не можете купить у него товар";
-- msgPriceLessThanZero = "The price is less than zero";
msgPriceLessThanZero = "Цена меньше 0";
msgRowsNotSet = "Не установлены поля. Установите все поля, пожалуйста";
msgSellerDoesNotHaveItemsCount = "У продавца закончился товар";
msgSellerNotFound = 'Продавец не найден';
msgSetResiverAndSum = "Укажите имя получателя и сумму";
msgSignWrongFormat = "Не верный формат указателя";
msgStringDelimer = "===============================================================================";
msgSumNotSet = "Укажите сумму";
msgTransactionAborted = "Транзакция отменена";
msgTransactionSuccess = "Транзакция успешна";
msgYourBalance = "Ваш баланс:";
msgYouBuySuccess = "Вам продали товар";
-- msgYouDoNotHaveBloks = "You do not have blocks";
msgYouDoNotHaveBloks = "У Вас нехватает товара";
msgYouSellItems = "Вы продали блоки";
msgReceiverDoesNotExists = "Получатель не найден или не верно указан ник"

--------------------
-- Console messages
--------------------
msgArgumentsAreNil = "Arguments are nil";
msgStorageInitError = "Database initialize error";
msgWorldsConNotInitialize = "Can not load world into DB";

