--
-- Storage:getPlayerID(plName);
--
--------------------------------------------------------------------------------

function cStorage:getPlayerID(aPlName)
-- Initialize an create DB

  local func_name = 'cStorage:getPlayerID()';

  if aPlName == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlName", 2);
    return true;
  end


  local plName = string.lower(aPlName);

  --
  local sql =[=[
    SELECT id
    FROM players
    WHERE login = :login
    ;
  ]=];

  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    login = plName
  });

  local id = 0;

  -- get value
  for id1 in stmt:urows() do
    id = id1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return id;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

