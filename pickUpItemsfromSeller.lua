--
--
--
--------------------------------------------------------------------------------

function pickUpItemsfromSeller(aWorld, aPlayerName, aItemID, aItemCount, aItemIDExtended)

  local func_name = 'pickUpItemsfromSeller()';

  local itemCount = aItemCount;
  local itemID = aItemID;
  local itemIDExtended = aItemIDExtended;
  local world = aWorld;

  local itemObj = cItem(itemID, 1, itemIDExtended);

  -- Check item create. Must be allways done
  if itemObj:IsEmpty() then
    return false;
  end

  -- Process player
  local isPlFound = world:DoWithPlayer(aPlayerName,
    function(aOwner)

      -- Get player inventory
      local plInventory = aOwner:GetInventory();

      -- Remove from inventory grid
      plInventory:RemoveItem(cItem(itemID, itemCount, itemIDExtended));

      aOwner:SendMessageSuccess(msgYouSellItems);

      return true;
    end
  );


end

