--
-- Plugin name: Money
-- Storage:setBalance(aPlayerID)
-- Return success staus (true or false)
--

function cStorage:setBalance(aPlayerID, aCurrencyID, aSum)

  local func_name = "cStorage:setBalance()";

  -- TODO: Message to console
  if not aPlayerID then
    return false;
  end

  local plID = aPlayerID;

  local sql = [[
    --
    -- Update balance into DB
    --
    UPDATE balances
    SET sum = :sum
    WHERE player_id = :plID AND currency_id = :currency_id
    ;
  ]];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plID = plID,
    currency_id = aCurrencyID,
    sum = aSum
  });

  -- Insert record
  local ret = stmt:step();

  if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
    console_log(func_name .." -> ret code = ".. ret, 2);
    stmt:finalize();
    db:close();
    return false;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Allright
  return true;
end

