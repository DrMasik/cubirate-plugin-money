--
-- Plugin name: Money
-- Storage:addWorlds()
-- Return success staus (true or false)
--

function cStorage:addWorlds()

  local func_name = "cStorage:addWorlds()";

  local worldName;

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  local sql = [[
    INSERT INTO worlds(name)
    VALUES(:worldName)
    ;
  ]];

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Process each world
  cRoot:Get():ForEachWorld(
    function(aWorld)

      worldName = aWorld:GetName():lower();

      -- Bind names
      stmt:bind_names({
        worldName = worldName
      });

      -- Insert record
      local ret = stmt:step();

    end
  );

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Allright
  return true;
end

