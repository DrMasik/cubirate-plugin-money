--
-- OnPlayerPlacingBlock(Player, BlockX, BlockY, BlockZ, BlockType, BlockMeta)
--
--------------------------------------------------------------------------------

function OnPlayerPlacingBlock(aPlayer, aBlockX, aBlockY, aBlockZ, aBlockType, aBlockMeta)

  local func_name = 'OnPlayerPlacingBlock()';

  -- Is it sign? 68 & 63
  if aBlockType ~= 68 and aBlockType ~= 63 then
    return false;
  end

  local signName = ';'.. aBlockX ..';'.. aBlockY ..';'.. aBlockZ ..';';
  local plName = aPlayer:GetName();
  local worldName = aPlayer:GetWorld():GetName();

  -- Create sign record for control
  gSign[signName] = cSign:new();
  gSign[signName]:setOwnerName(plName);
  gSign[signName]:setPosX(aBlockX);
  gSign[signName]:setPosY(aBlockY);
  gSign[signName]:setPosZ(aBlockZ);
  gSign[signName]:setWorldName(worldName);

  return false;
end

--------------------------------------------------------------------------------

