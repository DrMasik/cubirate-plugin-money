--
--
--
--------------------------------------------------------------------------------

function getPlayer(aWorld, aPlName)
  local func_name = 'getPlayer()';
  local player = nil;

  if aWorld == nil then
    return false;
  end

  local world = aWorld;

  world:DoWithPlayer(aPlName,
    function(aPlayer)
      player = aPlayer;
    end
  );

  return player;
end
--------------------------------------------------------------------------------

