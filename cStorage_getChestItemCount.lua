--
-- Plugin name: Money
-- cStorage:getChestItemCount()
--
--------------------------------------------------------------------------------

function cStorage:getChestItemCount(aWorldID, aPlID, aItemID, aItemIDExtended, aObjWorld)

  local func_name = "cStorage:getChestItemCount()";

  -- aObjWorld:BroadcastChat(func_name .." -> worldID = ".. aWorldID .."; plID = ".. aPlID); -- Debug

  -- Check arguments
  if aPlID == nil or aItemID == nil or aItemIDExtended == nil or aObjWorld == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil, 2);
    return false;
  end

  local worldID = aWorldID;
  local worldName = '';
  local objWorld = aObjWorld;
  local plID = aPlID;
  local itemID = aItemID;
  local itemIDExtended = aItemIDExtended;
  local itemCount = 0;
  local sql = '';

  -- Create item object
  local itemObj = cItem(itemID, 1, itemIDExtended);

  -- Check item creation
  if itemObj:IsEmpty() then
    console_log(func_name .." -> itemObj:IsEmpty() == true", 2);
    return false;
  end

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  sql = [[
    SELECT x, y, z
    FROM chests
    WHERE world_id = :worldID
          AND player_id = :plID
    ;
  ]];

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    worldID = worldID,
    plID = plID
  });

  -- Process chest
  for x1, y1, z1 in stmt:urows() do
    objWorld:DoWithChestAt(x1, y1, z1,
      function(aChestEntity)
        local itemGrid = aChestEntity:GetContents();

        itemCount = itemCount + itemGrid:HowManyItems(itemObj);
        return true;
      end
    );
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- aObjWorld:BroadcastChat(func_name .." -> itemCount = ".. itemCount); -- debug

  -- Allright
  return itemCount;
end

