--
-- Storage:getCurrncyBalance(plID, currencyID)
--
--------------------------------------------------------------------------------

function cStorage:getCurrncyBalance(aPlayerID, aCurrencyID)

  local func_name = 'cStorage:getCurrncyBalance()';

  local plID = aPlayerID;
  local currencyID = aCurrencyID;

  if plID == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." plID", 2);
    return true;
  end

  if currencyID == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." currencyID", 2);
    return true;
  end

  local sql =[=[
    SELECT sum
    FROM balances
    WHERE player_id = :plID
          AND currency_id = :currencyID
    ;
  ]=];

  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plID = plID,
    currencyID = currencyID
  });

  local sum = 0;

  -- Get value
  for sum1 in stmt:urows() do
    sum = sum1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return sum;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

