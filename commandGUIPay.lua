--
-- commandGUIPay
--
--------------------------------------------------------------------------------

function commandGUIPay(aSplit, aPlayer)
-- /money pay Nikolya 300 c
-- Pay to player Nikolya 300 credits
--
-- /money pay Nikolya 0.0003 b
-- Pay to player Nikolya 0.0003 bitcoints

  local func_name = 'commandGUIPay()';

  if aPlayer == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlayer", 2);
    return true;
  end

  if aSplit == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aSplit", 2);
    return true;
  end

  -- Check arguments count
  if #aSplit < 4 then
    aPlayer:SendMessageWarning(msgSetResiverAndSum);
    return true;
  end

  local payeeName = aSplit[3];

  -- Get payee ID
  local payeeID = gStorage:getPlayerID(payeeName);

  if payeeID == false or payeeID == 0 then
    aPlayer:SendMessageWarning(msgPayeeDoesNotExists);
    return true;
  end

  -- Get sum
  local sum = tonumber(aSplit[4]);

  -- Check money sum
  if sum == nil or sum == false or sum <= 0 then
    aPlayer:SendMessageWarning(msgSumNotSet);
    return true;
  end

  -- Get currency ID by currency code
  local currencyCodeName = 'c';
  local currencyID = gStorage:getCurrencyIDByCodeName(currencyCodeName);

  -- Get payer (player) ID
  local plName = aPlayer:GetName();
  local plID = gStorage:getPlayerID(plName);

  -- console_log(func_name .." -> plID = ".. plID .."; currencyID = ".. currencyID, 1);  -- Debug

  -- Check player balance for the currency
  local playerBalance = gStorage:getCurrncyBalance(plID, currencyID);

  if playerBalance == false then
    aPlayer:SendMessageWarning(msgCanNotGetCurrencyBalance);
    return true;
  end

  -- console_log(func_name .." -> sum = ".. sum .."; playerBalance = ".. playerBalance .."; Delta = ".. (tonumber(playerBalance) - tonumber(sum)), 1);  -- Debug

  -- Check balance ctatus after payment
  if (tostring(playerBalance) - tostring(sum)) < 0 then
    aPlayer:SendMessageWarning(msgDoNotHaveMoney);
    return true;
  end

  -- Create transaction
  if not gStorage:addTransaction(plID, payeeID, currencyID, tostring(sum)) then
    aPlayer:SendMessageWarning(msgTransactionAborted);
    return true;
  end

  aPlayer:SendMessageSuccess(msgTransactionSuccess);

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
