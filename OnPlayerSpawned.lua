--
--
--
--------------------------------------------------------------------------------

function OnPlayerSpawned(aPlayer)

  local plName = aPlayer:GetName();

  -- Check player balance exists
  local plID = gStorage:getPlayerID(plName);

  -- Is it player exists?
  if plID > 0 then
    return false;
  end

  -- Create balance record

  -- Create player record
  if not gStorage:addPlayer(plName) then
    return false;
  end

  -- Get player ID
  plID = gStorage:getPlayerID(plName);

  -- Check player ID
  if plID == false or plID == 0 then
    return false;
  end

  -- Create balance for the player
  gStorage:addBalance(plID);

  return false;
end

--------------------------------------------------------------------------------

