--
-- Plugin name: Money
-- Storage:getChestID()
-- Return success staus (true or false)
--

function cStorage:getChestID(aWorldID, aPosition)

  local func_name = "cStorage:getChestID()";

  -- Check arguments
  if aWorldID == nil or aPosition == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil, 2);
    return false;
  end

  local worldID = aWorldID;
  local position = aPosition;
  local id = nil;

  local sql = [[
    SELECT id
    FROM chests
    WHERE world_id = :worldID
          AND x = :x
          AND y = :y
          AND z = :z
    ;
  ]];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    worldID = worldID,
    x = position.x,
    y = position.y,
    z = position.z
  });

  -- Get chest ID
  for id1 in stmt:urows() do
    id = id1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Allright
  return id;
end

