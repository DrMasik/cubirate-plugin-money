--
--
--
--------------------------------------------------------------------------------

function cron()

  local func_name = "cron()";

  local secondsCount = 2;

  local ownerName = '';
  local posX = nil;
  local posY = nil;
  local posZ = nil;
  local worldName = '';
  local world = nil;
  local pos = {};
  local signClean = false;
  local isSignForBuy = false;

  -- Process records
  for id in pairs(gSign) do

    -- console_log(func_name .." -> process sign id = ".. id); -- Debug

    ownerName = gSign[id]:getOwnerName();
    worldName = gSign[id]:getWorldName();
    pos.x = gSign[id]:getPosX();
    pos.y = gSign[id]:getPosY();
    pos.z = gSign[id]:getPosZ();

    isSignForBuy = false;

    -- Get world as object
    world = cRoot:Get():GetWorld(worldName);

    -- Check world exists.
    if world ~= nil then

      -- Get sugn object
      local signExists, ln1, ln2, ln3, ln4 = world:GetSignLines(pos.x, pos.y, pos.z);

      -- Is it sign exists?
      if signExists then
        -- Check lines
        if ln1 == nil or ln2 == nil or ln3 == nil or ln4 == nil then
          -- Sign is clean and do not needed to check
        else

          -- Check sign format
          --if signCheckLine1(ln1) == false then
          if ln1 == '[sell]' then

            -- console_log(func_name ..' -> Sign is [sell]', 1);  -- Debug

            -- Process sell sign
            if signCheckSell(id) == false then
              -- Delete sign
              world:DigBlock(pos.x, pos.y, pos.z);
            end

            -- Delete from check class
            -- console_log(func_name ..' -> set signClean as true 1', 1);

            signClean = true;
          elseif ln1 == '[buy]' then

            -- console_log(func_name .." -> check sign line 2", 1);

            if signCheckLine2(ln2) == false then
              signClean = true;
            end

            -- console_log(func_name .." -> check sign line 3", 1);

            if signCheckLine3(ln3) == true then -- count:sum; There are sign for buy
            else
              signClean = true;
            end

            -- console_log(func_name .." -> check sign line 4", 1);

            if signCheckLine4(ln4) == true then -- seller name
              -- Check is it seller name == owner name
              -- If seller name != owner name - delete the sign

              -- console_log(func_name .." -> check sign owner", 1);
              if ownerName:lower() ~= ln4:lower() then
                -- console_log(func_name .." -> delete sign", 1);
                world:DigBlock(pos.x, pos.y, pos.z);
              end

              -- mar sign for delete from query
              signClean = true;
            else
              signClean = true;
            end
          -- else  -- It is simple sign
          --   console_log(func_name ..' -> set signClean as no rules', 1);  -- Debug
          --   signClean = true;
          end
        end
      else
        -- signClean = true;
      end -- if
    else
      console_log(func_name .." -> can not get world object by name = ".. worldName, 2);
      -- break;
    end -- if - else

    -- Delete record from memmory
    if signClean then
      -- console_log(func_name .." -> Delete sign ID: ".. id, 1);  --Debug
      gSign[id] = nil;
      signClean = false;
    end
  end -- for

  -- Schedule cron
  cRoot:Get():GetDefaultWorld():ScheduleTask(20 * secondsCount, cron);
end

--------------------------------------------------------------------------------

