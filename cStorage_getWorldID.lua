--
-- cStorage:getWorldID(aWorldName);
--
--------------------------------------------------------------------------------

function cStorage:getWorldID(aWorldName)
-- Initialize an create DB

  local func_name = 'cStorage:getWorldID()';

  if aWorldName == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil ..": aWorldName", 2);
    return false;
  end

  local worldName = aWorldName:lower();

  local sql =[=[
    SELECT id
    FROM worlds
    WHERE name = :name
    ;
  ]=];

  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    name = worldName
  });

  local id = 0;

  -- get value
  for id1 in stmt:urows() do
    id = id1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return id;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

