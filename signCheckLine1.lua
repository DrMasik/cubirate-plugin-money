--
--
--
--------------------------------------------------------------------------------

function signCheckLine1(aStr)
  local func_name = "signCheckLine1()";

  if aStr == nil then
    return false;
  end

  if type(aStr) ~= 'string' then
    return false;
  end

  if aStr:lower() ~= '[buy]' and aStr:lower() ~= '[sell]' then
    return false;
  end

  return true;
end

