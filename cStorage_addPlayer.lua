--
-- Plugin name: Money
-- Storage:addPlayer(aPlayerName)
-- Return success staus (true or false)
--

function cStorage:addPlayer(aPlayerName)

  local func_name = "cStorage:addPlayer()";

  -- TODO: Message to console
  if not aPlayerName then
    return false;
  end

  local plName = string.lower(aPlayerName);

  local sql = [[
    --
    -- Add player to DB
    --
    INSERT INTO players(login)
    VALUES(:plName)
    ;
  ]];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plName = plName;
  });

  -- Insert record
  local ret = stmt:step();

  if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
    console_log(func_name .." -> ret code = ".. ret, 2);
    stmt:finalize();
    db:close();
    return false;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Allright
  return true;
end

