--
-- commandGUIChestMark
--
--------------------------------------------------------------------------------

function commandGUIChestMark(aSplit, aPlayer)
  -- Activate mark status

  local func_name = 'commandGUIChestMark()';

  if aPlayer == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlayer", 2);
    return true;
  end

  if aSplit == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aSplit", 2);
    return true;
  end

  -- Set local variables
  local plName = aPlayer:GetName();
  local plID = gStorage:getPlayerID(plName);

  -- Check is it player ID detected?
  if plID == false or plID == 0 then
    aPlayer:SendMessageWarning(msgCanNotGetPlayerID);
    return true;
  end

  --
  -- Set mark status active
  --

  -- Is it mark activated?
  if gChestMark[plID] == nil then
    -- Create mark object as flag
    gChestMark[plID] = cChestMark:new();
  end

  -- Create mark object as flag
  gChestMark[plID] = cChestMark:new();

  aPlayer:SendMessage(msgStringDelimer);
  aPlayer:SendMessageSuccess(msgChestMarkStatusActivated);

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
