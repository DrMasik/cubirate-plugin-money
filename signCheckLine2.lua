--
-- signCheckLine2()
--
--------------------------------------------------------------------------------

function signCheckLine2(aStr)
  local func_name = "signCheckLine2()";

  if aStr == nil then
    return false;
  end

  if tonumber(aStr) == nil then
    return false;
  end

  return true;
end

