--
-- Plugin name: Money
-- Storage:deleteChest();
--

function cStorage:deleteChest(aChestID)

  local func_name = "cStorage:deleteChest()";

  -- Check arguments
  if aChestID == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil, 2);
    return false;
  end

  local chestID = aChestID;

  local sql = [[
    DELETE FROM chests
    WHERE id = :chestID
    ;
  ]];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    chestID = chestID
  });

  -- Insert record
  local ret = stmt:step();

  if ret ~= sqlite3.OK and ret ~= sqlite3.DONE then
    console_log(func_name .." -> ret code = ".. ret, 2);
    stmt:finalize();
    db:close();
    return false;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Allright
  return true;
end

