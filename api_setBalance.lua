--
-- api_setBalance
--
--------------------------------------------------------------------------------

function api_setBalance(aPlayerName, aCurrenyCodeName, aSum)
-- Set player's balance
-- Return transaction status
-- aCurrenyCodeName default must have 'c'
-- Function return code of error if return status false
--
-- Error codes:
-- 1 - Player name not set
-- 2 - Player balance not found
-- 3 - Sum not set
-- 4 - Balance set error
--
-- Simple function how to use
-- function commandGUISetBalance()
--   local status, value = cPluginManager:CallPlugin("Money", "api_setBalance", "DrMasik", "Credits", 17365)
--
--   if status == true then
--     console_log("TRUE")
--     console_log(value)
--   else
--     console_log("FALSE")
--     console_log("Returned error code: " + value)
--   end
-- end

  local func_name = 'api_setBalance()';

  local currencyCodeName = 'c';

  if aPlayerName == nil then
    return false, 1;
  end

  if aCurrenyCodeName ~= nil then
    currencyCodeName = aCurrenyCodeName;
  end

  if aSum == nil then
    return false, 3
  end

  local sum = math.abs(aSum);

  -- Set local variables
  local plName = aPlayerName;
  local plID = gStorage:getPlayerID(plName);

  -- Check is it player ID detected?
  if plID == false or plID == 0 then
    return false, 2;
  end

  -- Get currency ID by currency code
  local currencyID = gStorage:getCurrencyIDByCodeName(currencyCodeName);

  if not gStorage:setBalance(plID, currencyID, sum) then
    return false, 4;
  end

  return true, 0;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
