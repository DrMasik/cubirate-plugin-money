--
-- OnPlayerBreakingBlock(Player, BlockX, BlockY, BlockZ, BlockFace, BlockType, BlockMeta)
--
--------------------------------------------------------------------------------

function OnPlayerBreakingBlock(aPlayer, aBlockX, aBlockY, aBlockZ, aBlockFace, aBlockType, aBlockMeta)

  local func_name = "OnPlayerBreakingBlock()";

  local pos = {};
  local plName = aPlayer:GetName();
  local plID = gStorage:getPlayerID(plName);
  local worldID = gStorage:getWorldID(aPlayer:GetWorld():GetName());
  local chestID = nil;

  pos.x = aBlockX;
  pos.y = aBlockY;
  pos.z = aBlockZ;

  -- Check is it chest mark
  if gChestMark[plID] ~= nil then
    if aBlockType == 54 then
      -- Begin chest mark action

      -- Check is it chest exists
      chestID = gStorage:getChestID(worldID, pos);

      -- Add chest into DB
      if chestID == nil or chestID == false then
        gStorage:addChest(worldID, plID, pos);
        aPlayer:SendMessageSuccess(msgChestAdded);
      end
    end

    return true;
  end

  -- Get block type
  local signExists, ln1, ln2, ln3, ln4 = aPlayer:GetWorld():GetSignLines(pos.x, pos.y, pos.z);

  -- If it is not sign - exit
  if signExists == false then
    return false;
  end

  -- Check is it [buy] sign?
  if signCheckLine1(ln1) == false then
    return false;
  end

  -- Is it owner of sign?
  if signCheckLine4(ln4) == false then
    return false;
  end

  if ln4:lower() ~= aPlayer:GetName():lower() then
    return true;
  end

  return false;
end

