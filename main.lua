-- =============================================================================
--
-- Money
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org

gPluginName = '';

-------------------------------------------------------------------------------

function Initialize(Plugin)
  Plugin:SetName("Money")
  Plugin:SetVersion(20160821)

  gPluginName = Plugin:GetName();

  local pluginDir = cRoot:Get():GetPluginManager():GetCurrentPlugin():GetLocalFolder();

  console_log('-------------------------------------');
  console_log(gPluginName .. " initialize...")

  -- Load the InfoReg shared library:
  dofile(cPluginManager:GetPluginsPath() .. "/InfoReg.lua")

  -- Bind all the console commands:
  -- RegisterPluginInfoConsoleCommands()

  -- Bind all the commands (userspace):
  RegisterPluginInfoCommands()

  -- Load hooks
  hooks();

  -- Prepare storage for exports
  gStorage = cStorage:new(pluginDir, 'money.sqlite3');

  -- Create storage
  if not gStorage:init() then
    console_log(msgStorageInitError, 2);
    return false;
  end

  -- Add world if not exists
  if gStorage:addWorlds() == false then
    console_log(msgWorldsConNotInitialize, 2);
    return false;
  end

  -- Start cron
  cron();

  -- Nice message :)
  console_log(gPluginName ..": Initialized " .. gPluginName .. " v." .. Plugin:GetVersion())

  console_log('-------------------------------------');

  return true
end

-------------------------------------------------------------------------------

-- :%s/\s\+$//g

-- =============================================================================

