--
-- Storage:getBalance(plID, nil)
--
--------------------------------------------------------------------------------

function cStorage:getBalance(aPlayerID)

  local func_name = 'cStorage:getBalance()';

  local plID = aPlayerID;

  if plID == nil then
    return false;
  end

  if not tonumber(plID) then
    return false;
  end

  local sql =[=[
    SELECT currencies.name, balances.sum
    FROM balances, currencies
    WHERE balances.player_id = :plID
          AND currencies.id = balances.currency_id
    ;
  ]=];

  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    plID = plID
  });

  local balance = {};

  -- get value
  for currency_name1, sum1 in stmt:urows() do
    balance[currency_name1] = sum1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return balance;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

