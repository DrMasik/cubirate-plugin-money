--
-- commandGUIChestList
--
--------------------------------------------------------------------------------

function commandGUIChestList(aSplit, aPlayer)
  -- Activate mark status

  local func_name = 'commandGUIChestList()';

  if aPlayer == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlayer", 2);
    return true;
  end

  if aSplit == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aSplit", 2);
    return true;
  end

  -- Set local variables
  local plName = aPlayer:GetName();
  local worldName = aPlayer:GetWorld():GetName();
  local worldID = gStorage:getWorldID(worldName);
  local plID = gStorage:getPlayerID(plName);
  local chestList = nil;

  -- Show nice delimer
  aPlayer:SendMessage(msgStringDelimer);

  -- Check is it player ID detected?
  if plID == false or plID == 0 then
    aPlayer:SendMessageWarning(msgCanNotGetPlayerID);
    return true;
  end

  -- Get chest list with extended information
  chestList = gStorage:getChestList(worldID, plID);

  -- Check is it list exists?
  if chestList == nil or chestList == false then
    aPlayer:SendMessageSuccess(msgChestListAreEmpty);
    aPlayer:SendMessage(msgStringDelimer);
    return true;
  end

  ------------------------------
  -- Begin show list of chests -
  ------------------------------

  for id in pairs(chestList) do
    aPlayer:SendMessage(
      cCompositeChat()
      :AddTextPart(id .." ")
      :AddSuggestCommandPart("Delete", "/money chest delete ".. id, "u")
    );
  end

  -- Show nice delimer
  aPlayer:SendMessage(msgStringDelimer);

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
