--
-- Plugin name: Money
-- Storage:addTransaction(plID, payeeID, sum)
-- Return success staus (true or false)
--

function cStorage:addTransaction(aPlayerID, aPayeeID, aCurrencyID, aSum)

  local func_name = "cStorage:addTransaction()";

  local plID = aPlayerID;
  local payeeID = aPayeeID;
  local sum = tostring(aSum);
  local currencyID = aCurrencyID;

  if plID == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." plID", 2);
    return false;
  end

  if payeeID == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." payeeID", 2);
    return false;
  end

  if sum == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." sum", 2);
    return false;
  end

  if aCurrencyID == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aCurrencyID", 2);
    return false;
  end

  local sql1 = [[
    --
    -- Decrise balanse
    --
    UPDATE balances
    SET sum = (SELECT sum FROM balances WHERE player_id = :plID AND currency_id = :currencyID) - :sum
    WHERE player_id = :plID AND currency_id = :currencyID
    ;
  ]];

  local sql2 = [[
    --
    -- Add money to payee
    --
    UPDATE balances
    SET sum = :sum + (SELECT sum FROM balances WHERE player_id = :payeeID AND currency_id = :currencyID)
    WHERE player_id = :payeeID AND currency_id = :currencyID
    ;
  ]];

  local sql3 = [[
    --
    -- Add transaction to log
    --
    INSERT INTO transactions(pay_player_id, sell_player_id, currency_id, sum)
    VALUES(:plID, :payeeID, :currencyID, :sum)
    ;
  ]];

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt1 = db:prepare(sql1);

  -- Is it allright?
  if not stmt1 then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt1:bind_names({
    plID = plID,
    payeeID = payeeID,
    currencyID = currencyID,
    sum = sum
  });

  -- Execute statement
  local stmt2 = db:prepare(sql2);

  -- Is it allright?
  if not stmt2 then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt2:bind_names({
    plID = plID,
    payeeID = payeeID,
    currencyID = currencyID,
    sum = sum
  });

  -- Execute statement
  local stmt3 = db:prepare(sql3);

  -- Is it allright?
  if not stmt3 then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt3:bind_names({
    plID = plID,
    payeeID = payeeID,
    currencyID = currencyID,
    sum = sum
  });

  -- Begim transaction
  db:exec('BEGIN TRANSACTION;');

  -- Insert record
  local ret1 = stmt1:step();
  if ret1 ~= sqlite3.OK and ret1 ~= sqlite3.DONE then
    console_log(func_name .." -> ret1 code = ".. ret1, 2);
    stmt1:finalize();
    stmt2:finalize();
    stmt3:finalize();
    db:close();
    return false;
  end

  local ret2 = stmt2:step();
  if ret2 ~= sqlite3.OK and ret2 ~= sqlite3.DONE then
    console_log(func_name .." -> ret2 code = ".. ret2, 2);
    stmt1:finalize();
    stmt2:finalize();
    stmt3:finalize();
    db:close();
    return false;
  end

  local ret3 = stmt3:step();
  if ret3 ~= sqlite3.OK and ret3 ~= sqlite3.DONE then
    console_log(func_name .." -> ret3 code = ".. ret3, 2);
    stmt1:finalize();
    stmt2:finalize();
    stmt3:finalize();
    db:close();
    return false;
  end

  -- Commit transaction
  db:exec('END TRANSACTION;');

  -- Clean handler
  stmt1:finalize();
  stmt2:finalize();
  stmt3:finalize();

  -- Close DB
  db:close();

  -- Allright
  return true;
end

