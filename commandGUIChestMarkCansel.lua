--
-- commandGUIChestMarkCanselCansel
--
--------------------------------------------------------------------------------

function commandGUIChestMarkCansel(aSplit, aPlayer)
  -- Activate mark status

  local func_name = 'commandGUIChestMarkCansel()';

  if aPlayer == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlayer", 2);
    return true;
  end

  if aSplit == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aSplit", 2);
    return true;
  end

  -- Set local variables
  local plName = aPlayer:GetName();
  local plID = gStorage:getPlayerID(plName);

  -- Check is it player ID detected?
  if plID == false or plID == 0 then
    aPlayer:SendMessageWarning(msgCanNotGetPlayerID);
    return true;
  end

  --
  -- Clean mark status
  --

  -- Is it mark activated?
  gChestMark[plID] = nil;

  aPlayer:SendMessageSuccess(msgChestMarkStatusDeactivated);
  aPlayer:SendMessage(msgStringDelimer);

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
