--
-- signProcessR3(aString)
--
--------------------------------------------------------------------------------

function signProcessR3(aString)
  func_name = 'signProcessR3()';

  if aString == nil then
    return false;
  end

  local r3 = tostring(aString);
  local delimerPos = 0;
  local itemCount = 0;
  local itemPrice = 0;

  -- Is it delimer exists?
  delimerPos = r3:find(':');

  -- Get item code and extended item code
  if delimerPos == nil or delimerPos == 0 then
    itemCount = tonumber(r3);
    itemPrice = 0;
  else
    itemCount = tonumber(r3:sub(1, delimerPos-1)); -- Code of item
    itemPrice = tonumber(r3:sub(delimerPos+1));  -- Extended code of item

    if itemPrice == nil or itemPrice == false then
      itemPrice = 0;
    end
  end

  -- Return item code and item extended code
  return itemCount, itemPrice;
end
--------------------------------------------------------------------------------

