--
--
--
--------------------------------------------------------------------------------

function cStorage:new(aFileDir, aFileName)
  local object = {};

  object.fileName = aFileName;
  object.fileDir = aFileDir;
  object.filePath = aFileDir .."/".. aFileName;

  setmetatable(object, self);

  self.__index = self;

  return object;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

