--
--
--
--------------------------------------------------------------------------------

function worldFind(aName)
  local func_name = 'worldFind()';

  local world = nil;

  if aName == nil then
    -- console_log(func_name .." -> aName == nil", 1);  -- Debug
    return false;
  end

  cRoot:Get():ForEachWorld(
    function(aWorld)
      if aWorld:GetName():lower() == aName:lower() then
        world = aWorld;
        return true;
      end
    end
  );

  return world;
end
