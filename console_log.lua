-- =============================================================================
--
-- Money
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org

gShowConsoleDebugMessages = true;  -- Disable debug messages to console

-------------------------------------------------------------------------------

function console_log(a_str, msg_type)
-- 0 - standart
-- 1 - debug
-- 2 - error

  local str = '';

  if msg_type == nil then
    msg_type =0;
  end

  if msg_type == 0 then
    str = a_str;
  elseif msg_type == 1 then
    if gShowConsoleDebugMessages then
      str = "Debug: ".. a_str;
    end
  else
    str = "Error: ".. a_str;
  end

  if str ~= '' then
    LOG(gPluginName ..": ".. str);
  end
end

-------------------------------------------------------------------------------

-- :%s/\s\+$//g

-- =============================================================================
