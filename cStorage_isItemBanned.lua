--
-- Storage:isItemBanned(itemID)
--
--------------------------------------------------------------------------------

function cStorage:isItemBanned(aItemID)

  local func_name = 'cStorage:isItemBanned()';

  local itemID = aItemID;

  if itemID == nil then
    return false;
  end

  if not tonumber(itemID) then
    return false;
  end

  local sql =[=[
    SELECT COUNT(*)
    FROM banned_items
    WHERE item_id = :itemID
    ;
  ]=];

  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    itemID = itemID
  });

  local count = 0;

  -- get value
  for count1 in stmt:urows() do
    count = count1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Item not banned
  if count == 0 then
    return false;
  end

  -- Item banned
  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

