g_PluginInfo = {
  Name = "Money",
  Version = "20160821",
  Date = "2016-08-21",
  Description = [=[ Money ]=],

  ConsoleCommands =
  {
  },

--------------------------------------------------------------------------------

  Commands = {
    ['/money'] = {
      Permission = "money.core",
      HelpString = "Money plugn",

      Subcommands = {
        set = {
          Alias = {'s'},
          HelpString = "Set some plugin data",
          Permission = "money.set",

          Subcommands = {
            balance = {
              Alias = {'b'},
              HelpString = "Set balans for the player",
              Handler = commandGUISetBalance,
            },

            encBalance = {
              Alias = {'e'},
              HelpString = "Encrease balance",
              Handler = commandGUISetEncBalance,
            },

            decBalance = {
              Alias = {'d'},
              HelpString = "Decrease balance",
              Handler = commandGUISetDecBalance,
            },
          }, -- set Subcommands
        },  -- set

        balance = {
          Alias = {'b'},
          HelpString = "View blance",
          Handler = commandGUIBalance,
        },  -- balance

        pay = {
          Alias = {'p'},
          HelpString = "Pay to player",
          Handler = commandGUIPay,
        },  -- Pay

        mark = {
          Alias = {'m'},
          HelpString = "Mark chest",
          Handler = commandGUIChestMark
        },

        mc = {
          HelpString = "Cancel mark chest",
          Handler = commandGUIChestMarkCansel,
        },

        chest = {
          Alias = {'c'},
          HelpString = "Chest manipulation",

          Subcommands = {
            list = {
                Alias = {'l'},
                HelpString = "Список сундуков",
                Handler = commandGUIChestList,
              },  -- list

            delete = {
              Alias = {'d'},
              HelpString = "Удалить сундук из списка",
              Handler = commandGUIChestDelete,
            },  -- delete
          },  -- Subcommands
        },  -- chest

        -- sell = {
        --   Alias = {'s'},
        --   HelpString = "Sell items",
        --   Handler = commandGUISell,
        -- },  -- Sell

        -- buy = {
        --   HelpString = "Buy items",
        --   Handler = commandGUIBuy,
        -- },
      },  -- Subcommands
    },  -- /money
  },  -- Commands

--------------------------------------------------------------------------------

} -- g_PluginInfo

