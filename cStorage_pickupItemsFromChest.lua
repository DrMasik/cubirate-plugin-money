--
-- Plugin name: Money
-- cStorage:pickupItemsFromChest
--
--------------------------------------------------------------------------------

function cStorage:pickupItemsFromChest(aWorldID, aPlID, aItemID, aItemCountToPickupFromChest, aItemIDExtended, aObjWorld, aObjPayer)

  local func_name = "cStorage:pickupItemsFromChest()";

  -- Check arguments
  if aPlID == nil or aItemID == nil or aItemIDExtended == nil or aObjWorld == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil, 2);
    return false;
  end

  local worldID = aWorldID;
  local worldName = '';
  local objWorld = aObjWorld;
  local plID = aPlID;
  local itemID = aItemID;
  local itemIDExtended = aItemIDExtended;
  local itemCount = aItemCountToPickupFromChest;
  local objPlayer = aObjPayer;
  local sql = '';
  local itemCountPickup = 0;

  -- Create item object
  local itemObj = cItem(itemID, itemCount, itemIDExtended);

  -- Check item creation
  if itemObj:IsEmpty() then
    return false;
  end

  -- Open DB
  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  sql = [[
    SELECT x, y, z
    FROM chests
    WHERE world_id = :worldID
          AND player_id = :plID
    ;
  ]];

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    worldID = worldID,
    plID = plID
  });

  -- Set counter
  itemCountProcess = itemCount;

  -- Process chest
  for x1, y1, z1 in stmt:urows() do
    objWorld:DoWithChestAt(x1, y1, z1,
      function(aChestEntity)
        local itemGrid = aChestEntity:GetContents();

        itemCountProcess = itemCountProcess - itemGrid:RemoveItem(itemObj);

        return true;
      end
    );

    -- Is it all pickuped?
    if itemCountProcess < 1 then
      break;
    end

    -- Update item object
    itemObj = cItem(itemID, itemCountProcess, itemIDExtended);
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  -- Allright
  return itemCountProcess;
end

