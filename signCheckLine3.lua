--
-- signCheckLine3(aStr)
--
--------------------------------------------------------------------------------

function signCheckLine3(aStr)
  local func_name = "signCheckLine3()";

  if aStr == nil then
    return false;
  end

  if type(aStr) ~= 'string' then
    return false;
  end

  -- Get delimer position
  local delimerPos = aStr:find(':');

  -- Is it right format?
  if delimerPos == nil then
    return false;
  end

  -- Get items count
  local itemCount = tonumber(aStr:sub(1, delimerPos-1));

  -- Check item count right
  if itemCount == false then
    return false;
  end

  -- Get sum
  local sum = tonumber(aStr:sub(delimerPos+1));

  -- Check sum
  if sum == false or sum == nil then
    return false;
  end

  return true;
end

