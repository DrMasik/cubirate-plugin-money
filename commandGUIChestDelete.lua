--
-- commandGUIChestDelete
--
--------------------------------------------------------------------------------

function commandGUIChestDelete(aSplit, aPlayer)
  -- Activate mark status

  local func_name = 'commandGUIChestDelete()';

  if aPlayer == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlayer", 2);
    return true;
  end

  if aSplit == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aSplit", 2);
    return true;
  end

  -- Check arguments
  if #aSplit < 4 then
    aPlayer:SendMessageInfo(msgChestDeleteUsage);
    return true;
  end

  -- Set local variables
  local chestID = tonumber(aSplit[4]);
  local isDeleted = nil;

  -- Show nice delimer
  aPlayer:SendMessage(msgStringDelimer);

  -- Check is it player ID detected?
  if chestID == nil or chestID == false then
    aPlayer:SendMessageWarning(msgChestIDNotFound);
    return true;
  end

  -- Get chest list with extended information
  isDeleted = gStorage:deleteChest(chestID);

  if isDeleted == nil or isDeleted == false then
    aPlayer:SendMessageWarning(msgChestNotDeleted);
  else
    aPlayer:SendMessageSuccess(msgChestDeleteSuccess);
  end

  -- Show nice delimer
  aPlayer:SendMessage(msgStringDelimer);

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------

