--
-- Storage:getCurrencyIDByCodeName(aCurrencyCodeName)
--
--------------------------------------------------------------------------------

function cStorage:getCurrencyIDByCodeName(aCurrencyCodeName)

  local func_name = 'cStorage:getCurrencyIDByCodeName()';

  local currencyCodeName = aCurrencyCodeName;

  if currencyCodeName == nil then
    return false;
  end

  local sql =[=[
    SELECT id
    FROM currencies
    WHERE code = :currencyCodeName
    ;
  ]=];

  local db = sqlite3.open(self.filePath);

  if not db then
    console_log(func_name .."-> Can not open DB ".. self.filePath, 2);
    return false;
  end

  -- Execute statement
  local stmt = db:prepare(sql);

  -- Is it allright?
  if not stmt then
    console_log(func_name .." -> db:prepare is nil", 2);
    db:close();
    return false;
  end

  -- Bind names
  stmt:bind_names({
    currencyCodeName = currencyCodeName
  });

  local id = 0;

  -- Get value
  for id1 in stmt:urows() do
    id = id1;
  end

  -- Clean handler
  stmt:finalize();

  -- Close DB
  db:close();

  return id;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

