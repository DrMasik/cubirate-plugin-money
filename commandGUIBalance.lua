--
-- commandGUIBalance
--
--------------------------------------------------------------------------------

function commandGUIBalance(aSplit, aPlayer)
  local func_name = 'commandGUIBalance()';

  if aPlayer == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aPlayer", 2);
    return true;
  end

  if aSplit == nil then
    console_log(func_name .." -> ".. msgArgumentsAreNil .." aSplit", 2);
    return true;
  end

  -- Set local variables
  local plName = aPlayer:GetName();
  local plID = gStorage:getPlayerID(plName);

  -- Check is it player ID detected?
  if plID == false or plID == 0 then
    aPlayer:SendMessageWarning(msgCanNotGetPlayerID);
    return true;
  end

  -- Get balance list
  local balanceList = gStorage:getBalance(plID, nil);

  -- Is it balance exists
  if balanceList == false then
    aPlayer:SendMessageWarning(CanNotGetCurrencies);
    return true;
  end

  -- Show header
  aPlayer:SendMessage("===================================================");
  aPlayer:SendMessage(msgYourBalance);

  -- Show list of currencies
  for currencyName in pairs(balanceList) do
    aPlayer:SendMessage(currencyName ..": ".. balanceList[currencyName]);
  end

  aPlayer:SendMessage("===================================================");

  return true;
end

--------------------------------------------------------------------------------

-- :%s/\s\+$//g

--------------------------------------------------------------------------------
