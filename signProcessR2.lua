--
-- signProcessR2(aString)
--
--------------------------------------------------------------------------------

function signProcessR2(aString)
  func_name = 'signProcessR2()';

  if aString == nil then
    return false;
  end

  local r2 = tostring(aString);
  local delimerPos = 0;
  local itemID = 0;
  local itemIDExtended = 0;

  -- Is it delimer exists?
  delimerPos = r2:find('/');

  -- Get item code and extended item code
  if delimerPos == nil or delimerPos == 0 then
    itemID = tonumber(r2);
    itemIDExtended = 0;
  else
    itemID = tonumber(r2:sub(1, delimerPos-1)); -- Code of item
    itemIDExtended = tonumber(r2:sub(delimerPos+1));  -- Extended code of item

    if itemIDExtended == nil or itemIDExtended == false then
      itemIDExtended = 0;
    end
  end

  -- Return item code and item extended code
  return itemID, itemIDExtended;
end
--------------------------------------------------------------------------------

