--
-- OnPlayerRightClick(Player, BlockX, BlockY, BlockZ, BlockFace, CursorX, CursorY, CursorZ)
--
--------------------------------------------------------------------------------

function OnPlayerRightClick(aPlayer, aBlockX, aBlockY, aBlockZ, BlockFace, CursorX, CursorY, CursorZ)

  local func_name = "OnPlayerRightClick()";

  -- Check arguments
  -- if aPlayer == nil or aBlockX == nil or aBlockY == nil or aBlockZ == nil then
  --   console_log(func_name .." -> ".. msgArgumentsAreNil, 2);
  --   return false;
  -- end

  local blockCoordinates = ";".. aBlockX ..";".. aBlockY ..";".. aBlockZ ..";";

  local world = aPlayer:GetWorld();

  local itemID = 0;
  local itemCount = 0;
  local itemExtCodePos = 0;
  local itemIDExtended = 0;
  local itemCountProcess = 0;
  local chestItemCount = 0;
  local payer   = nil;
  local plName  = '';
  local plID    = '';
  local payeeID = '';
  local delimerPos = 0;
  local itemCountLeft = 0;
  local objWorld = aPlayer:GetWorld();
  local worldName = aPlayer:GetWorld():GetName();
  local worldID = 0;

  -- Get block type
  local isSign, r1, r2, r3, r4 = world:GetSignLines(aBlockX, aBlockY, aBlockZ);

  if not isSign then
    return false;
  end

  -- Check fields
  if r1 == nil or r2 == nil or r3 == nil or r4 == nil then
    return false;
  end

  -- Convert to lower case
  r1 = r1:lower();

  -- Is it processed sign?
  if r1 == '[sell]' then
    if processSell(aPlayer, Vector3i(aBlockX, aBlockY, aBlockZ)) then
      return true;
    end

    return true;
  elseif r1 ~= '[buy]' then
    return false;
  end

  -- Get extended code of item
  itemExtCodePos = tostring(r2):find('/');

  -- Get item code and extended item code
  if itemExtCodePos == nil or itemExtCodePos == 0 then
    itemID = tonumber(r2);
    itemIDExtended = 0;
  else
    itemID = tonumber(r2:sub(1, itemExtCodePos-1)); -- Code of item
    itemIDExtended = tonumber(r2:sub(itemExtCodePos+1));  -- Extended code of item

    if itemIDExtended == nil or itemIDExtended == false then
      itemIDExtended = 0;
    end
  end

  -- Is it number for the item?
  if itemID == false or itemID == nil then
    aPlayer:SendMessageWarning(msgItemDisabled);
    return true;
  end

  -- Check item banned
  if gStorage:isItemBanned(itemID) then
    aPlayer:SendMessageWarning(msgItemDisabled);
    return true;
  end

  -- Get delimer position
  delimerPos = r3:find(':');

  -- Is it right format?
  if delimerPos == nil then
    aPlayer:SendMessageWarning(msgSignWrongFormat);
    return true;
  end

  -- Get items count
  itemCount = tonumber(r3:sub(1, delimerPos-1));
  itemCountLeft = itemCount;

  -- Check item count right
  if itemCount == false or itemCount == nil then
    aPlayer:SendMessageWarning(msgItemCountWrong);
    return true;
  end

  -- Get sum
  local sum = tonumber(r3:sub(delimerPos+1));

  -- Check sum
  if sum == nil or sum == false then
    aPlayer:SendMessageWarning(msgItemCountWrong);
    return true;
  end

  -- Get owner name
  local owner = r4;

  -- Is it server's bank?
  if owner:lower() == 'bank of m-c.link' then
    processBuyBank(aPlayer, Vector3i(aBlockX, aBlockY, aBlockZ));
    return true;
  end

  -- Item count into seller
  local itemCountCurr = 0;

  -- Chek is it owner online
  local payeeFound = world:DoWithPlayer(owner,
    function(aOwner)
      -- Check is it owner has items

      local plInventory = aOwner:GetInventory();

      itemCountCurr = plInventory:HowManyItems(cItem(itemID, 1, itemIDExtended));

      payer = aOwner;

      return true;
    end
  );

  plName  = aPlayer:GetName();
  plID    = gStorage:getPlayerID(plName);
  payeeID = gStorage:getPlayerID(owner);
  worldID = gStorage:getWorldID(worldName);

  -- Is it payer and payee exists?
  if plID == 0 or payeeID == 0 or plID == false or payeeID == false then
    aPlayer:SendMessageWarning(msgPayeeDoesNotExists);
    return true;
  end

  -- Get items from players chest
  chestItemCount = gStorage:getChestItemCount(worldID, payeeID, itemID, itemIDExtended, objWorld);

  -- aPlayer:SendMessage(func_name .." -> chestItemCount = ".. chestItemCount);  -- Debug
  -- aPlayer:SendMessage(func_name .." -> itemCountCurr = ".. itemCountCurr);  -- Debug

  -- Is it items get?
  if chestItemCount == false then
    chestItemCount = 0;
  end

  -- Check count of items into seller
  if itemCount > (itemCountCurr + chestItemCount) then
    aPlayer:SendMessageWarning(msgSellerDoesNotHaveItemsCount);
    return true;
  end

  -- Get currency ID by currency code
  local currencyCodeName = 'c';
  local currencyID = gStorage:getCurrencyIDByCodeName(currencyCodeName);

  -- Get player balance
  local balanceCurr = tostring(gStorage:getCurrncyBalance(plID, currencyID));

  -- Check player money
  if (balanceCurr - tostring(sum)) < 0 then
    aPlayer:SendMessageWarning(msgDoNotHaveMoney);
    return true;
  end

  -- Create item object
  local itemObj = cItem(itemID, itemCount, itemIDExtended);

  -- Check item creation
  if itemObj:IsEmpty() then
    aPlayer:SendMessageWarning(msgItemCodeWrong);
    return true;
  end

  -- Create transaction
  if not gStorage:addTransaction(plID, payeeID, currencyID, sum) then
    aPlayer:SendMessageWarning(msgTransactionAborted);
    return true;
  end

  -- Pickup items from seller's chest
  if chestItemCount > 0 then

    itemCountProcess = math.min(itemCount, chestItemCount);

    -- Pickup items from chest
    itemCountLeft = gStorage:pickupItemsFromChest(worldID, payeeID, itemID, itemCountProcess, itemIDExtended, objWorld, payer);

    if itemCountLeft == nil or itemCountLeft == false then
      aPlayer:SendMessageWarning("Some error");
      console_log(func_name .." -> itemCountLeft == nil or false", 2);
      return true;
    end

    -- Calculate items to process
    itemCountLeft = itemCount - itemCountProcess + itemCountLeft;
  end

  -- aPlayer:SendMessage(func_name .." -> itemCountLeft = ".. itemCountLeft);

  -- Get items from seller
  if itemCountLeft > 0 then
    pickUpItemsfromSeller(world, owner, itemID, itemCountLeft, itemIDExtended);
  end

  -- Add items to player
  aPlayer:GetInventory():AddItem(itemObj);

  -- Message to buyer(payer)
  aPlayer:SendMessageSuccess(msgTransactionSuccess);

  -- Send message to seller
  -- aPlayer:SendMessage(tostring(gStorage:getCurrncyBalance(plID, currencyID)));

  return true;
end

--------------------------------------------------------------------------------

